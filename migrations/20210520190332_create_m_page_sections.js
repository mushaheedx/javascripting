
exports.up = function (knex) {
    let creationTimeFn = knex.fn.now()

    return knex.schema.createTable('m_page_sections', function (table) {
        table.increments('section_id')
                .unsigned()
                .primary();
        table.integer('page_id')
                .unsigned()
                .notNullable();
        table.string('section_title')
                .notNullable();
        table.string('section_description')
                .notNullable();
        table.boolean('is_section_active')
                .notNullable()
                .defaultTo(1);
        table.string('created_by');
        table.timestamp('created_at')
                .defaultTo(creationTimeFn);
        table.string('updated_by');
        table.timestamp('updated_at')
                .defaultTo(creationTimeFn);
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable('m_page_sections');
};
