
exports.up = function (knex) {
        let creationTimeFn = knex.fn.now()

        return knex.schema.createTable('m_pages', function (table) {
                table.increments('page_id')
                        .unsigned()
                        .primary();
                table.string('page_title')
                        .notNullable();
                table.string('page_description')
                        .notNullable();
                table.boolean('is_page_active')
                        .notNullable()
                        .defaultTo(1);
                table.string('created_by');
                table.timestamp('created_at')
                        .defaultTo(creationTimeFn);
                table.string('updated_by');
                table.timestamp('updated_at')
                        .defaultTo(creationTimeFn);
        });
};

exports.down = function (knex) {
        return knex.schema.dropTable('m_pages');
};
