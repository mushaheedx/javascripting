'use strict';

import responseService from '@/app/services/response_service';
import statusType from '@/app/enum/status_type';

// common utils


export const getCurrentTimestamp = (request) => {
    return request.app.knexConnection.fn.now();
}

class TriedTaskResult {
    constructor(result, error) {
        this.result = result;
        this.error = error;
    }
    get hasResult() {
        return hasValue(this.result);
    }
    get hasError() {
        return hasValue(this.error);
    }
    static onError(error) {
        return new TriedTaskResult(null, error);
    }
    static onResult(result) {
        return new TriedTaskResult(result, null);
    }
}

export async function tryTaskAsync(
    task,
) {
    try {
        let result = await task();
        return TriedTaskResult.onResult(result);
    } catch (error) {
        console.log('ERROR: ', error);
        return TriedTaskResult.onError(error);
    }
}

// Only use this to wrap module logic
export async function tryFulfil(
    response,
    task,
    responseStatusType = statusType.INTERNAL_SERVER_ERROR,
    message = "Something went wrong",
    version = 'v1.0'
) {
    let result = await tryTaskAsync(task);
    if (result.hasError) {
        return responseService.sendResponse2(response, null, responseStatusType, message, version);
    } else {
        return result.result;
    }
}

export const resultForDbError = (response, errorMessage) => {
    return responseService.sendResponse2(response, null, statusType.DB_ERROR, errorMessages.dbError(errorMessage), 'v1.0');
}

export let errorMessages = {
    dbError: (value) => `database error occurred on '${value}' operation`,
}

export function hasValue(value) {
    return value != undefined && value != null && (typeof value == 'string' ? value !== '' : true);
}

export function getFirstOrNull(value) {
    if (!Array.isArray(value)) throw TypeError(`argument passed is not an array, it is ${typeof value}`);
    if (value.length == 0) return null;
    return value[0];
}
