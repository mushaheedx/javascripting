import statusType from "@/app/enum/status_type";
import * as constant from "@/app/helpers/constant";

function getErrorMessage(code) {
    switch (code) {
        case statusType.BAD_REQUEST:
            // BAD_REQUEST
            // CODE: 400
            return { statusCode: statusType.BAD_REQUEST, status: false, type: "Error", message: constant.RESPONSE_MESSAGES[`CODE_${statusType.BAD_REQUEST}`] };
        case statusType.UNAUTHORIZED:
            // UNAUTHORIZED
            // 401
            return { status: false, type: "Error", message: constant.RESPONSE_MESSAGES[`CODE_${statusType.UNAUTHORIZED}`] };
        case statusType.FORBIDDEN:
            // FORBIDDEN
            // 403
            // "Requested operation is not allowed due to applied rules. Please refer to error details."
            return { status: false, type: "Error", message: constant.RESPONSE_MESSAGES[`CODE_${statusType.FORBIDDEN}`] };
        case statusType.NOT_FOUND:
            // NOT_FOUND
            // 404
            return { status: false, type: "Error", message: constant.RESPONSE_MESSAGES[`CODE_${statusType.NOT_FOUND}`] };
        case statusType.METHOD_NOT_ALLOWED:
            // METHOD_NOT_ALLOWED
            // 405
            return { status: false, type: "Error", message: constant.RESPONSE_MESSAGES[`CODE_${statusType.METHOD_NOT_ALLOWED}`] };
        case statusType.REQUEST_TIME_OUT:
            // REQUEST_TIME_OUT
            // 408
            return { status: false, type: "Error", message: constant.RESPONSE_MESSAGES[`CODE_${statusType.REQUEST_TIME_OUT}`] };
        case statusType.INTERNAL_SERVER_ERROR:
            // INTERNAL_SERVER_ERROR
            // 500
            return { status: false, type: "Error", message: constant.RESPONSE_MESSAGES[`CODE_${statusType.INTERNAL_SERVER_ERROR}`] };
        case statusType.NOT_IMPLEMENTED_UNAUTHORIZED:
            // NOT_IMPLEMENTED_UNAUTHORIZED
            // 501
            return { status: false, type: "Error", message: constant.RESPONSE_MESSAGES[`CODE_${statusType.NOT_IMPLEMENTED_UNAUTHORIZED}`] };
        case statusType.SERVICE_UNAVAILABLE:
            // SERVICE_UNAVAILABLE
            // 503
            return { status: false, type: "Error", message: constant.RESPONSE_MESSAGES[`CODE_${statusType.SERVICE_UNAVAILABLE}`] };
        case statusType.DB_ERROR:
            // DB_ERROR
            // 422
            return { status: false, type: "Error", message: constant.RESPONSE_MESSAGES[`CODE_${statusType.DB_ERROR}`] };
        case statusType.SUCCESS:
            // SUCCESS
            // 200
            return { status: true, type: "OK", message: constant.RESPONSE_MESSAGES[`CODE_${statusType.SUCCESS}`] };
        case statusType.CREATED:
            // CREATED
            // 201
            return { status: true, type: "OK", message: constant.RESPONSE_MESSAGES[`CODE_${statusType.CREATED}`] };
        default:
            return { status: false, type: "Error", message: constant.RESPONSE_MESSAGES[`CODE_${statusType.REQUEST_TIME_OUT}`] };
    };
};

module.exports = getErrorMessage;
