const geoip = require("geoip-lite");

module.exports.sendCountry = async (req, res) => {
  let knex = req.app.knexConnection("ms_countries");
  
  // use geocode
  let ip = req.meta.clientIp;
  let geo = geoip.lookup(ip);
  console.log("geo", geo);
  const { country } = geo;
  console.log("country", country);
  const countries = await knex;
  const ip_country = await knex.where({ country });

  // Send all countries and ip_country
  return res.status(200).json({ countries, ip_country });
};
