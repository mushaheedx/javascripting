import ResponseService from "@/app/services/response_service";
import statusType from "@/app/enum/status_type";
import LoggingService from "@/app/services/logging_service";

import * as constant from "@/app/helpers/constant";
import SecurityClient from "@/app/security_service/security_client";

module.exports.Testing = (req, res) => {
    let apiVersion = req["meta"] ? req.meta["version"] || null : null;
    LoggingService.consoleLog(`TESTING", "Here we are on ${apiVersion}`, {
        error: "error",
    });
    // Get List From the Queries and pass it to the response service
    let data = [{ data: "data" }];

    return res.status(statusType.SUCCESS).json(ResponseService.sendResponse(statusType.SUCCESS, constant.CUSTOM_RESPONSE_MESSAGES.USER_RES, apiVersion, data));
};


module.exports.login = (req, res) => {
    let apiVersion = req["meta"] ? req.meta["version"] || null : null;
    //Get data from users and create json object to encrypt
    let jwt_obj = { user_id: 1, user_name: 'John Doe' }

    const encryptToken = SecurityClient.encrypt(JSON.stringify(jwt_obj))
    const token = SecurityClient.jwtEncode({ encryptToken })
    return res.status(statusType.SUCCESS).json(ResponseService.sendResponse(statusType.SUCCESS, constant.CUSTOM_RESPONSE_MESSAGES.USER_RES, apiVersion, token));

}