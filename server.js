const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const morgan = require('morgan');
const http = require('http');
const path = require('path');

// ENABLE MODULE ALIAS
import 'module-alias/register';
// require("./moduleAliases"); // Testing

// USE CUSTOM MODULES
import * as countryMiddleware from '@/middleware/country_middleware';
import errorHandler from '@/middleware/error_handling';
import * as constant from '@/app/helpers/constant';
import LoggingService from '@/app/services/logging_service';

const port = constant.config.port || 8002;

const app = express();
const server = http.createServer(app);


const knex = require('knex')(require('@/config/index.js').knexConnection);
const { attachPaginate } = require('knex-paginate');

attachPaginate();
app.knexConnection = knex;

app.set('port', process.env.PORT || port);

// Static folder
app.use(express.static(path.join(__dirname, 'public'), { maxage: '7d' }));

// view engine
app.set('view engine', 'ejs');
app.use(cors());
global.__base = __dirname;
app.use(bodyParser.json({ limit: '500mb' }));
app.use(bodyParser.urlencoded({ extended: false, limit: '500mb' }));
app.use(cookieParser());
morgan.token('process-ip', function (req) {
    return (
        req.headers['cf-connecting-ip'] ||
        req.headers['x-forwarded-for'] ||
        req.headers['x-real-ip'] ||
        req.ip ||
        ''
    );
});

app.use(
    morgan(
        ':process-ip - :date - ":method :url HTTP/:http-version" - :status - :res[content-length] - :response-time ms',
        {
            stream: {
                write: function (msg) {
                    return LoggingService.consoleLog('MORGAN', msg);
                },
            },
        }
    )
);

app.use(
    fileUpload({ limits: { fileSize: 5 * 1024 * 1024 }, safeFileNames: false, abortOnLimit: true })
);

app.use(countryMiddleware.setCountry());

// app.use(response);
// ENABLE OR INITIATE ROUTES
require('@/routes').default(app);

app.use(errorHandler);

server.listen(app.get('port') || 8002, '0.0.0.0');

const onError = (error) => {
    if (error.syscall !== 'listen') throw error;
    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(`${error.code}: Port ${port} requires elevated privileges`);
            process.exit(1);
        case 'EADDRINUSE':
            console.error(`${error.code}: Port ${port} is already in use`);
            process.exit(1);
        default:
            throw error;
    }
};

const onListening = () => {
    let addr = server.address();
    let bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
    console.log('Server Listening on ' + bind);
};

server.on('error', onError);
server.on('listening', onListening);
