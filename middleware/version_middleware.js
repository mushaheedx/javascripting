export const setRequestVersion = (version) => {
    return (req, res, next) => {
        req['meta'] = {
            ...req['meta'],
            version
        };
        return next();
    };
}