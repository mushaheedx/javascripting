const publicIp = require("public-ip");

export const setCountry = () => {
  return async (req, res, next) => {
    //TODO-pending - Attach IP
    // const clientIp = requestIp.getClientIp(req);
    const clientIp = await publicIp.v4();
    //Add country here, possible to check db for country/url and not hard code
    if (req.get("host") == "ksa.cinepolis.com") {
      req["meta"] = {
        ...req["meta"],
        country: "ksa",
        country_id: 1,
        clientIp,
      };
    } else {
      req["meta"] = {
        ...req["meta"],
        country: "ksa",
        country_id: 1,
        clientIp,
      };
    }

    return next();
  };
};
