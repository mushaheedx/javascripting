const fs = require("fs");
const path = require("path");

import * as versionMiddleware from "@/middleware/version_middleware";
import apiVersions from "@/app/enum/api_versions";
const versionRoutePath = path.join(__dirname, "");

const initRoute = (app) => {
  app.use('/api/', require(`@/microservices/routes`));
  app.use('/api/country', require(`@/app/lib/country_code`).sendCountry);
  
  fs.readdirSync(versionRoutePath).map((x) => {
    if (apiVersions[x.replace(/\./g, "_").toUpperCase()]) app.use(`/api/${x}`, versionMiddleware.setRequestVersion(x), require(`@/routes/${x}`));
  });
};
export default initRoute;
