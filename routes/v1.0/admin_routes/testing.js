const express = require("express");
const router = express.Router();
import AdminController from "@/controllers/v1.0/admin_controller";

router.route("/testing/:id").get(AdminController.Testing);

router.route("/testing").get(AdminController.Testing);

router.route('/login').get(AdminController.login);

module.exports = router;
