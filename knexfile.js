module.exports = {
    development: {
        client: 'mysql',
        connection: {
            host: 'localhost',
            port: 3306,
            user: 'root',
            password: 'D1ct10n@2Y.',
            database: 'NPLJDB',
        },
        pool: {
            min: 2,
            max: 10,
        },
    },
};
