'use strict';

import castTinyIntAsBoolean from '@/app/helpers/cutils';

export const port = 8002;
export const external_port = 8002;
export const protocol = 'http://';
export const host = '127.0.0.1';
export const environment = 'development';
export const SERVICE_NAME = 'admin-platform';
export const FRONTEND_URL = 'https://ott-frontend.binarynumbers.io/';

export const mongodb = { uri: 'mongodb' };

export const options = {
	reconnectTries: 100,
	reconnectInterval: 500,
	autoReconnect: true,
	//useNewUrlParser: true,
	dbName: 'platform',
};

// SET
export const utils = {
	CRON_WORKER: '*/2 * * * * *',
	PASSWORD_SALT: 'BnNodeBoilerplate_AdnjssckjFasDkmsRakld',
	JWT_SECRET: 'BnNodeBoilerplate_GtvhkLRTgtNdGl',
	JWT_TOKEN_EXPIRE: 2 * 86000, // 2 day

	ENCRYPT_SALT: 'BnNodeBoilerplate_GtvhkLRTgtNdGl',
	// ENCRYPT_TOKEN_EXPIRE: 2 * 86000, // 2 day
};

export const secret = {
	NODE_TOKEN_SECRET: 'Basic dGVzdGluZ0F1dGg6dGVzdGluZ0BBdXRo',
};

export const amazon = {
	bucketName: 'platform-auxy',
	imageUrl: 'https://s3.us-east-1.amazonaws.com',
};

// This must be same as /knexfile.js
export const knexConnection = {
	client: 'mysql',
	connection: {
		host: 'localhost',
		port: 3306,
		user: 'root',
		password: 'D1ct10n@2Y.',
		database: 'NPLJDB',
		typeCast: (field, next) => {
			if (field.type == 'TINY' && field.length == 1) {
				// tinyint to boolean
				let value = field.string();
				return value ? (value == '1') : null;
			}
			return next();
		},
	},
	pool: {
		min: 2,
		max: 10,
	},
};

export const brightCove = {
	account_id: '6219356502001',
	client_id: 'eda42e42-535c-4237-ab68-978319f251e4',
	access_token:
		'32udRgtazTZRuA-xu5Vnd2BEyC5UEvtnKDB_Y1wjhaCP3d-zfv4xKTjyX3h4bY2aJMFRuebLsyxH8eOGIlZfpg',
};

export const s3Credentials = {
	accessKeyId: 'AKIA2DTZUJXAV6REPQFE',
	secretAccessKey: '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',
	region: 'us-east-1',
};

export const razorpayKey = {
	key_id: 'rzp_test_wfQlc6sPdbMsXq',
	key_secret: 'hSnUimAiNz6EMERC7xthh8Bp',
	varification_secret: '789456123',
};

module.exports.mailDetails = {
	secure: true, // true for 465, false for other ports
	host: 'smtp.gmail.com',
	port: 465,
	transportMethod: 'SMTP',
	from: 'OTT <saif@binarynumbers.io>',
	auth: {
		user: 'saif@binarynumbers.io', // generated ethereal user
		pass: 'fias.@687*', // generated ethereal password
		// user: 'developers@binarynumbers.io',
		// pass: 'ewfwej3n3f'
	},
};
