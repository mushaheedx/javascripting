# STEPS

1. Copied base configuration, services, helpers, enums.

2. Created migration file with `npx knex migrate:make create_m_pages`

3. Wrote below up/down migrations in generated file.
```js
exports.up = function (knex) {
    let creationTimeFn = knex.fn.now()

    return knex.schema.createTable('m_page_sections', function (table) {
        table.increments('section_id')
                .unsigned()
                .primary();
        table.int('page_id')
                .unsigned()
                .notNullable();
        table.string('section_title')
                .notNullable();
        table.string('section_description')
                .notNullable();
        table.boolean('is_section_active')
                .notNullable()
                .defaultTo(1);
        table.string('created_by');
        table.timestamp('created_at')
                .defaultTo(creationTimeFn);
        table.string('updated_by');
        table.timestamp('updated_at')
                .defaultTo(creationTimeFn);
    });
};

exports.down = function (knex) {
    return knex.schema.dropTable('m_page_sections');
};
```

4. Verify migrations in mysql cli with commands:
```SQL
USE NPLJDB;
SHOW TABLES;
DESCRIBE m_page_sections;
```

5. results of above commands
```sql
+---------------------+--------------+------+-----+-------------------+-------------------+
| Field               | Type         | Null | Key | Default           | Extra             |
+---------------------+--------------+------+-----+-------------------+-------------------+
| section_id          | int unsigned | NO   | PRI | NULL              | auto_increment    |
| page_id             | int unsigned | NO   |     | NULL              |                   |
| section_title       | varchar(255) | NO   |     | NULL              |                   |
| section_description | varchar(255) | NO   |     | NULL              |                   |
| is_section_active   | tinyint(1)   | NO   |     | 1                 |                   |
| created_by          | varchar(255) | YES  |     | NULL              |                   |
| created_at          | timestamp    | YES  |     | CURRENT_TIMESTAMP | DEFAULT_GENERATED |
| updated_by          | varchar(255) | YES  |     | NULL              |                   |
| updated_at          | timestamp    | YES  |     | CURRENT_TIMESTAMP | DEFAULT_GENERATED |
+---------------------+--------------+------+-----+-------------------+-------------------+
9 rows in set (0.00 sec)
```

6. add code in routes.js, modules/page/page_routes.js, modules/page/page_controller.js

7. perform table join between m_pages & m_page_sections and match m_pages.created_by with params.userId
```js
return table.select(
            'm_pages.page_id',
            'm_pages.page_title',
            'm_pages.page_description',
            'm_pages.created_by',
            'm_pages.created_at',
    
            'm_page_sections.section_id',
            'm_page_sections.page_id',
            'm_page_sections.section_title',
            'm_page_sections.section_description',
            'm_page_sections.is_section_active',
            'm_page_sections.created_by',
            'm_page_sections.created_at',
        )
        .innerJoin(
            'm_page_sections',
            'm_page_sections.page_id',
            'm_pages.page_id'
        )
        .where({ 
            'm_pages.created_by': user_id,
            is_page_active: true,
        });
```

8. Return this result as a successful response.

9. Use curl to make request: `curl localhost:8002/api/page/by/2;`
```json
{"status":true,"apiVersion":"v1.0","statusCode":200,"type":"OK","message":"Success","data":[]}
```

10. List of all API Requests and their results:

1)

```bash
curl -X POST -H "Content-Type: application/json" \
    -d '{"page_title": "Sample Title", "page_description": "Sample page description", "user_id": "mushaheedx"}' \
    localhost:8002/api/page/add
```

```json
{
    "status": true,
    "apiVersion": "v1.0",
    "statusCode": 200,
    "type": "OK",
    "message": "Success",
    "data": null
}}
```

2)
```bash
curl -X PUT -H "Content-Type: application/json" \
    -d '{"page_id":"", "page_title": "Sample Title", "page_description": "Sample page description", "user_id": "mushaheedx"}' \
    localhost:8002/api/page/update/1
```

```json
{
    "status": true,
    "apiVersion": "v1.0",
    "statusCode": 200,
    "type": "OK",
    "message": "Success",
    "data": null
}
```

3)
```bash
curl localhost:8002/api/page/only/1;
```

```json
{
    "status": true,
    "apiVersion": "v1.0",
    "statusCode": 200,
    "type": "OK",
    "message": "Success",
    "data": {
        "page_id": 1,
        "page_title": "Sample Title",
        "page_description": "Sample page description",
        "is_page_active": true,
        "created_by": "mushaheedx",
        "created_at": "2021-05-21T19:51:06.000Z",
        "updated_by": "mushaheedx",
        "updated_at": "2021-05-21T19:56:25.000Z"
    }
}
```

4)
```bash
curl localhost:8002/api/page/all
```

```json
{
    "status": true,
    "apiVersion": "v1.0",
    "statusCode": 200,
    "type": "OK",
    "message": "Success",
    "data": [
        {
            "page_id": 1,
            "page_title": "Sample Title",
            "page_description": "Sample page description",
            "is_page_active": true,
            "created_by": "mushaheedx",
            "created_at": "2021-05-21T19:51:06.000Z",
            "updated_by": "mushaheedx",
            "updated_at": "2021-05-21T19:56:25.000Z"
        },
        {
            "page_id": 2,
            "page_title": "Sample Title",
            "page_description": "Sample page description",
            "is_page_active": true,
            "created_by": "mushaheedx",
            "created_at": "2021-05-21T19:52:48.000Z",
            "updated_by": null,
            "updated_at": "2021-05-21T19:52:48.000Z"
        }
    ]
}
```

5)
```bash
curl -X POST -H "Content-Type: application/json" \
    -d '{"page_id": 1, "section_title": "Section Title 1", "section_description": "Page section description", "user_id": "mushaheedx"}' \
    localhost:8002/api/page/section/add
```

```json
{
    "status": true,
    "apiVersion": "v1.0",
    "statusCode": 200,
    "type": "OK",
    "message": "Success",
    "data": null
}
```

6)
```bash
curl -X PUT -H "Content-Type: application/json" \
    -d '{"page_id": 1, "section_title": "Sample Title", "section_description": "Sample page description", "user_id": "mushaheedx"}' \
    localhost:8002/api/page/section/update/1
```

```json
{
    "status": true,
    "apiVersion": "v1.0",
    "statusCode": 200,
    "type": "OK",
    "message": "Success",
    "data": null
}
```

7)
```bash
curl localhost:8002/api/page/by/mushaheedx
```

```json
{
    "status": true,
    "apiVersion": "v1.0",
    "statusCode": 200,
    "type": "OK",
    "message": "Success",
    "data": [
        {
            "page_id": 1,
            "page_title": "Sample Title",
            "page_description": "Sample page description",
            "created_by": "mushaheedx",
            "created_at": "2021-05-21T20:00:14.000Z",
            "section_id": 1,
            "section_title": "Sample Title",
            "section_description": "Sample page description",
            "is_section_active": true
        }
    ]
}
```
