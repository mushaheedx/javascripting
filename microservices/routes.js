'use strict';

import pageRoutes from '@/microservices/modules/page/page_routes';

const express = require('express');
const router = express.Router();

router.use('/page', pageRoutes);

module.exports = router;