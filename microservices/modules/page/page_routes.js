'use strict';

import pageController from '@/microservices/modules/page/page_controller';

const express = require('express');
const router = express.Router();

// curl localhost:8002/api/page/all 
router.route('/all').get(pageController.getAllPages);

// curl localhost:8002/api/page/only/1 
router.route('/only/:page_id').get(pageController.getOnly);

// curl localhost:8002/api/page/by/mushaheedx
router.route('/by/:user_id').get(pageController.getPagesByCreator);

// curl -X POST -H "Content-Type: application/json" \
//     -d '{"page_title": "Sample Title", "page_description": "Sample page description", "user_id": "mushaheedx"}' \
//     localhost:8002/api/page/add
router.route('/add').post(pageController.addPage);

// curl -X PUT -H "Content-Type: application/json" \
//     -d '{"page_id":"", "page_title": "Sample Title", "page_description": "Sample page description", "user_id": "mushaheedx"}' \
//     localhost:8002/api/page/update/1
router.route('/update/:page_id').put(pageController.updatePage);

// curl -X POST -H "Content-Type: application/json" \
//     -d '{"page_id": 1, "section_title": "Section Title 1", "section_description": "Page section description", "user_id": "mushaheedx"}' \
//     localhost:8002/api/page/section/add
router.route('/section/add').post(pageController.addSection);

// curl -X PUT -H "Content-Type: application/json" \
//     -d '{"page_id": 1, "section_title": "Sample Title", "section_description": "Sample page description", "user_id": "mushaheedx"}' \
//     localhost:8002/api/page/section/update/1
router.route('/section/update/:section_id').put(pageController.updateSection);

module.exports = router;
