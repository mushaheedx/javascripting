'use strict';

import responseService from '@/app/services/response_service';
import statusType from '@/app/enum/status_type';

import { hasValue, getFirstOrNull, getCurrentTimestamp } from '@/app/helpers/cutils';

// GET
module.exports.getPagesByCreator = async (
    request,
    response,
) => {
    try {
        const user_id = request.params.user_id;

        if (!hasValue(user_id)) {
            return responseService.sendResponse2(response, null, statusType.BAD_REQUEST, "user_id is blank or null");
        }

        let data = await request.app.knexConnection('m_pages').select(
            'm_pages.page_id',
            'm_pages.page_title',
            'm_pages.page_description',
            'm_pages.created_by',
            'm_pages.created_at',

            'm_page_sections.section_id',
            'm_page_sections.page_id',
            'm_page_sections.section_title',
            'm_page_sections.section_description',
            'm_page_sections.is_section_active',
            'm_page_sections.created_by',
            'm_page_sections.created_at',
        )
            .innerJoin(
                'm_page_sections',
                'm_page_sections.page_id',
                'm_pages.page_id'
            )
            .where({
                'm_pages.created_by': user_id,
                is_page_active: true,
            });

        return responseService.sendResponse2(response, data, statusType.SUCCESS, null, 'v1.0');
    } catch (error) {
        console.error(error);
        return responseService.sendResponse2(response, null, statusType.INTERNAL_SERVER_ERROR, "Something went wrong", 'v1.0');
    }
};

// GET
module.exports.getAllPages = async (
    request,
    response,
) => {
    try {
        let data = await request.app.knexConnection('m_pages').select('*');

        return responseService.sendResponse2(response, data, statusType.SUCCESS, null, 'v1.0');
    } catch (error) {
        console.error(error);
        return responseService.sendResponse2(response, null, statusType.INTERNAL_SERVER_ERROR, "Something went wrong", 'v1.0');
    }
};

/**
 * 
 * @param {int} page_id 
 * @returns {Promise<object>}
 */
const _getSinglePage = (request, page_id) => {
    return request.app.knexConnection('m_pages')
        .select('*')
        .where({
            'page_id': page_id,
        });
}

// GET
module.exports.getOnly = async (
    request,
    response,
)  => {
    try {
        let pageId = request.params.page_id;

        if (!hasValue(pageId)) {
            return responseService.sendResponse2(response, null, statusType.BAD_REQUEST, "page_id is blank or null", 'v1.0');
        }

        let data = await _getSinglePage(request, pageId);

        let result = getFirstOrNull(data);

        if (!hasValue(result)) {
            return responseService.sendResponse2(response, result, statusType.SUCCESS, `No page exists with page_id: ${pageId}`, 'v1.0');
        }

        return responseService.sendResponse2(response, result, statusType.SUCCESS, null, 'v1.0');
    } catch (error) {
        console.error(error);
        return responseService.sendResponse2(response, null, statusType.INTERNAL_SERVER_ERROR, "Something went wrong", 'v1.0');
    }
};

// POST
module.exports.addPage = async (
    request,
    response,
) => {
    try {
        let is_params_ok = hasValue(request.body);
        let page_title;
        let page_description;
        let created_by;

        if (is_params_ok) {
            page_title = request.body.page_title;
            page_description = request.body.page_description;
            created_by = request.body.user_id;

            is_params_ok = hasValue(page_title) && hasValue(page_description);
        }

        if (!is_params_ok) {
            return responseService.sendResponse2(response, null, statusType.BAD_REQUEST, "page_title or page_description is blank or null", 'v1.0');
        }

        let data = await request.app.knexConnection('m_pages')
            .insert({
                'page_title': page_title,
                'page_description': page_description,
                'created_by': created_by || null,
            });

        return responseService.sendResponse2(response, data ? true : false, statusType.SUCCESS, null, 'v1.0');
    } catch (error) {
        console.error(error);
        return responseService.sendResponse2(response, null, statusType.INTERNAL_SERVER_ERROR, "Something went wrong", 'v1.0');
    }

};

// PUT
module.exports.updatePage = async (
    request,
    response,
) => {
    try {
        let is_params_ok = hasValue(request.params);
        let page_id;

        if (is_params_ok) {
            page_id = request.params.page_id;
            is_params_ok = hasValue(page_id);
        }

        if (!is_params_ok) {
            return responseService.sendResponse2(response, null, statusType.BAD_REQUEST, "page_id is blank or null", 'v1.0');
        }

        let needs_change = hasValue(request.body);
        let page_title;
        let page_description;
        let is_page_active;
        let updated_by;

        if (needs_change) {
            page_title = request.body.page_title;
            page_description = request.body.page_description;
            updated_by = request.body.user_id;
            is_page_active = request.body.is_page_active;

            needs_change = hasValue(page_title) || hasValue(page_description) || hasValue(is_page_active);
        }

        if (!needs_change) {
            return responseService.sendResponse2(response, null, statusType.SUCCESS, 'No changes made', 'v1.0');
        }

        let data = await request.app.knexConnection('m_pages')
            .update({
                'page_title': page_title,
                'page_description': page_description,
                'is_page_active': is_page_active,
                'updated_by': updated_by || null,
                'updated_at': getCurrentTimestamp(request),
            })
            .where({
                'page_id': page_id,
            });

        if (!data) {
            return responseService.sendResponse2(response, false, statusType.SUCCESS, `No changes made. Page with page_id: ${page_id} does not exist.`, 'v1.0');
        }

        return responseService.sendResponse2(response, true, statusType.SUCCESS, null, 'v1.0');
    } catch (error) {
        console.error(error);
        return responseService.sendResponse2(response, null, statusType.INTERNAL_SERVER_ERROR, "Something went wrong", 'v1.0');
    }

};

// POST
module.exports.addSection = async (
    request,
    response,
) => {
    try {
        let is_params_ok = hasValue(request.body);
        let page_id;
        let section_title;
        let section_description;
        let created_by;

        if (is_params_ok) {
            page_id = request.body.page_id;
            section_title = request.body.section_title;
            section_description = request.body.section_description;
            created_by = request.body.user_id;

            is_params_ok = hasValue(page_id) && hasValue(section_title) && hasValue(section_description);
        }

        if (!is_params_ok) {
            return responseService.sendResponse2(response, null, statusType.BAD_REQUEST, "page_id, section_title or section_description is blank or null", 'v1.0');
        }

        let page_result = await _getSinglePage(request, page_id);

        if (!hasValue(getFirstOrNull(page_result))) {
            return responseService.sendResponse2(response, null, statusType.BAD_REQUEST, `page with page_id ${page_id} does not exist`, 'v1.0');
        }

        let data = await request.app.knexConnection('m_page_sections')
            .insert({
                'page_id': page_id,
                'section_title': section_title,
                'section_description': section_description,
                'created_by': created_by || null,
            });

        return responseService.sendResponse2(response, data ? true : false, statusType.SUCCESS, null, 'v1.0');

    } catch (error) {
        console.error(error);
        return responseService.sendResponse2(response, null, statusType.INTERNAL_SERVER_ERROR, "Something went wrong", 'v1.0');
    }
};


// PUT
module.exports.updateSection = async (
    request,
    response,
) => {
    try {
        let is_params_ok = hasValue(request.params);
        let section_id;

        if (is_params_ok) {
            section_id = request.params.section_id;
            is_params_ok = hasValue(section_id);
        }

        if (!is_params_ok) {
            return responseService.sendResponse2(response, null, statusType.BAD_REQUEST, "section_id is blank or null", 'v1.0');
        }

        let needs_change = hasValue(request.body);
        let page_id;
        let section_title;
        let section_description;
        let is_section_active;
        let updated_by;

        if (needs_change) {
            page_id = request.body.page_id;
            section_title = request.body.section_title;
            section_description = request.body.section_description;
            is_section_active = request.body.is_section_active;
            updated_by = request.body.user_id;

            needs_change = hasValue(page_id) || hasValue(section_title) || hasValue(section_description) || hasValue(is_section_active);
        }

        if (!needs_change) {
            return responseService.sendResponse2(response, null, statusType.SUCCESS, 'No changes made', 'v1.0');
        }

        let data = await request.app.knexConnection('m_page_sections')
            .update({
                'page_id': page_id,
                'section_title': section_title,
                'section_description': section_description,
                'is_section_active': is_section_active,
                'updated_by': updated_by || null,
                'updated_at': getCurrentTimestamp(request),
            })
            .where({
                'section_id': section_id,
            });

        if (!data) {
            return responseService.sendResponse2(response, false, statusType.SUCCESS, `No changes made. Page section with section_id: ${section_id} does not exist.`, 'v1.0');
        }

        return responseService.sendResponse2(response, true, statusType.SUCCESS, null, 'v1.0');
    } catch (error) {
        console.error(error);
        return responseService.sendResponse2(response, null, statusType.INTERNAL_SERVER_ERROR, "Something went wrong", 'v1.0');
    }
};
